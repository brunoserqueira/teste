Como utilizar o serviço de produtos:
    
NECESSÁRIO RODAR A APLICAÇÃO ATRAVÉS DO MAVEN PARA BAIXAR SUAS DEPENDENCIAS.


METODO:http://localhost:8080/testeAc/rest/produtos/getAll
EXEMPLO: -
PARAMETROS: NENHUM

DESCRICAO: RECUPERA TODOS PRODUTOS COM SUAS RESPECTIVAS DEPENDENCIAS
========================================================================================
METODO: http://localhost:8080/testeAc/rest/produtos/listAllChildrenByProduct/{idProduct}
EXEMPLO: http://localhost:8080/testeAc/rest/produtos/listAllChildrenByProduct/1
PARAMETRO: ID DO PRODUTO

DESCRIÇÃO: RECUPERA TODOS PRODUTOS FILHOS DE UM PRODUTO ESPECIFICO, DEFINIDO ATRAVÉS DE SEU ID.
========================================================================================
METODO: METODO:http://localhost:8080/testeAc/rest/produtos/getAllImageByProduct/{idProduct}
EXEMPLO: http://localhost:8080/testeAc/rest/produtos/getAllImageByProduct/1
PARAMETROS: ID DO PRODUTO

DESCRIÇÃO: RECUPERA TODAS IMAGENS DE UM PRODUTO ESPECIFICO
 ========================================================================================
METODO: http://localhost:8080/testeAc/rest/produtos/getByIdWithDependencias/{idProduct}
EXEMPLO:http://localhost:8080/testeAc/rest/produtos/getByIdWithDependencias/1
PARAMETROS: ID DO PRODUTO(VALOR NUMERICO)

DESCRIÇÃO: RECUPERA UM PRODUTO ESPECIFICO, DEFINIDO ATRAVÉS DE SEU ID, COM TODAS SUAS RESPECTIVAS DEPENDENCIAS
 ========================================================================================
METODO: http://localhost:8080/testeAc/rest/produtos/removeProduct/{idProduct}
EXEMPLO:http://localhost:8080/testeAc/rest/produtos/removeProduct/1
PARAMETROS: ID DO PRODUTO(VALOR NUMERICO)

DESCRIÇÃO: REMOVE UM PRODUTO ESPECIFICO, DEFINIDO ATRAVÉS DE SEU ID
 ========================================================================================
METODO: http://localhost:8080/testeAc/rest/produtos/saveProduct/{name}/{description}
EXEMPLO:http://localhost:8080/testeAc/rest/produtos/saveProduct/tenis/esportivo
PARAMETROS: NOME E DESCRICAO DO PRODUTO ( AMBOS STRING)

DESCRIÇÃO: INSERE UM NOVO PRODUTO NO BANCO, RECEBENDO OS SEUS RESPECTIVOS CAMPOS DE NOME E DESCRICAO
 ========================================================================================
METODO: http://localhost:8080/testeAc/rest/produtos/editProduct/{id}/{name}/{description}
EXEMPLO:http://localhost:8080/testeAc/rest/produtos/editProduct/1/sapato/social
PARAMETROS: ID DO PRODUTO(VALOR NUMERICO) NOME E DESCRICAO DO PRODUTO ( AMBOS STRING)

DESCRIÇÃO: ALTERA UM PRODUTO EXISTENTE NO BANCO, ATRAVES DO SEU ID, RECEBENDO OS SEUS RESPECTIVOS CAMPOS DE NOME E DESCRICAO
========================================================================================
METODO: http://localhost:8080/testeAc/rest/produtos/saveImage/{idProduct}/{type}
EXEMPLO:http://localhost:8080/testeAc/rest/produtos/saveImage/1/tipoTeste
PARAMETROS: ID DO PRODUTO(VALOR NUMERICO) AO QUAL A IMAGEM PERTENCE E TIPO DA IMAGEM (VALOR STRING)

DESCRIÇÃO: INSERE UMA NOVA IMAGEM NO BANCO, ASSOCIANDO-A A UM PRODUTO PRE-EXISTENTE, E INSERINDO O SEU TIPO DE IMAGEM
========================================================================================
METODO: http://localhost:8080/testeAc/rest/produtos/editImage/{id}/{type}/{idProduct}
EXEMPLO:http://localhost:8080/testeAc/rest/produtos/editImage/1/tipoTeste/1
PARAMETROS: ID DA IMAGEM(VALOR NUMERICO) QUE SERA EDITADA, TIPO DA IMAGEM (VALOR STRING) E ID DO PRODUTO(VALOR NUMERICO) 

DESCRIÇÃO: EDITA UMA  IMAGEM EXISTENTE NO BANCO, É INFORMADO O ID DA IMAGEM QUE DESEJA ALTERAR, O NOVO VALOR PARA TIPO E O ID DO PRODUTO, CASO DESEJE ALTERAR O PRODUTO AO QUAL A IMAGEM PERTENCE    
========================================================================================
METODO: http://localhost:8080/testeAc/rest/produtos/removeImagem/{id} 
EXEMPLO:http://localhost:8080/testeAc/rest/produtos/removeImagem/1 
PARAMETROS: ID DA IMAGEM(VALOR NUMERICO) QUE SERA REMOVIDA 

DESCRIÇÃO: REMOVE UMA  IMAGEM EXISTENTE NO BANCO, É INFORMADO O ID DA IMAGEM QUE DESEJA EXCLUIR    
    
    
    
PENDENCIAS:
INFELIZMENTE, NÃO CONSEGUI UM TEMPO SUFICIENTE PARA IMPLEMENTAR TUDO E DA MELHOR FORMA, PORTANTO PRIORIZEI A CRIAÇÃO DAS TASKS, E OS TESTES FICARAM PENDENTES.
  
package br.com.ac.teste.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@NamedQueries({
    @NamedQuery(name = "Product.getByIdWithImages", query = "SELECT p FROM Product p LEFT JOIN FETCH p.images WHERE p.id = :id"),
    @NamedQuery(name = "Product.getByIdWithSubProducts", query = "SELECT p FROM Product p LEFT JOIN FETCH p.subProducts WHERE p.id = :id"),
    @NamedQuery(name = "Product.getAllWithSubProducts", query = "SELECT p FROM Product p LEFT JOIN FETCH p.subProducts"),
    @NamedQuery(name = "Product.getAllWithImages", query = "SELECT p FROM Product p LEFT JOIN FETCH p.images "),
    @NamedQuery(name = "Product.getAll", query = "SELECT p FROM Product p LEFT JOIN FETCH p.subProducts "),
    @NamedQuery(name = "Product.getProductChildrenByIdProduct", query = "SELECT p FROM Product p WHERE p.produto.id = :idProdutoPai ")
 
})
@Entity
@XmlRootElement
public class Product {
	
	public Product() {
	}
	public Product(Long id) {
		this.id = id;
	}
	public Product(String name, String description) {
		this.name = name;
		this.description = description;
	}
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID")
	private Long id;
	@Column(name="NAME")
	private String name;
	@Column(name="DESCRIPTION")
	private String description;
	
	@JoinColumn(name="PARENT_ID_PRODUCT")
	@ManyToOne
	private Product produto;
	@OneToMany(mappedBy="produto")
	
	private List<Product> subProducts;
	
	@JsonIgnore
	@OneToMany(mappedBy="product",fetch=FetchType.LAZY)
	private List<Image> images;
	 
	
	 
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Product> getSubProducts() {
		return subProducts;
	}
	public void setSubProducts(List<Product> subProducts) {
		this.subProducts = subProducts;
	}
	public List<Image> getImages() {
		return images;
	}
	public void setImages(List<Image> images) {
		this.images = images;
	}
	public Product getProduto() {
		return produto;
	}
	public void setProduto(Product produto) {
		this.produto = produto;
	}
	
	
}

package br.com.ac.teste.repository;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.ac.teste.model.Image;
import br.com.ac.teste.model.Product;

@Stateless
@Local
public class ImageRepository {

	@PersistenceContext(unitName = "hsqldb")
    private EntityManager em;

    public Image findById(Long id) {
        return em.find(Image.class, id);
    }
    @SuppressWarnings("unchecked")
	public List<Image> findImagesByProduct(Long idProduct) {
        Query query = em.createNamedQuery("Image.getImagesByProduct");
        query.setParameter("idProduct", idProduct);
        return query.getResultList();
		
    }
    public Image save(Image image) {
    	image.setProduct(em.find(Product.class, image.getProduct().getId()));
    	return em.merge(image);
    }
    public void remove(Long id) {
    	Image imageForRemoval = em.find(Image.class, id);
    	em.remove(imageForRemoval);
    }
   
}

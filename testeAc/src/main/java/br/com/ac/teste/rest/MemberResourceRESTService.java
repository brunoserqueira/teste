
package br.com.ac.teste.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.ac.teste.dto.ProductDTO;
import br.com.ac.teste.model.Image;
import br.com.ac.teste.model.Product;
import br.com.ac.teste.repository.ImageRepository;
import br.com.ac.teste.repository.ProductRepositoty;

@Path("/produtos")
@RequestScoped
public class MemberResourceRESTService {

 

    @EJB
    private ProductRepositoty ProductRepository;
 
    @EJB
    private ImageRepository imageRepository;

    @GET
    @Path("getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProductDTO> listAllProducts() {
        return ProductRepository.findAll();
        
    }
    @GET
    @Path("getAllChildrenByProduct/{idProduct}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Product> listAllChildrenByProduct(@PathParam("idProduct") String idProduct) {
        return ProductRepository.findAllChildrenByIdProduct(Long.parseLong(idProduct));
        
    }
    @GET
    @Path("getAllImageByProduct/{idProduct}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Image> listAllImageByProduct(@PathParam("idProduct") String idProduct){
    	return imageRepository.findImagesByProduct(Long.parseLong(idProduct));
    	 
    }
    @GET
    @Path("getByIdWithDependencias/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public ProductDTO getProductById(@PathParam("id") long id) {
        ProductDTO product = ProductRepository.findByIdWithDependencies(id);
        if (product == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return product;
    }
    @GET
    @Path("removeById/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeProduct(@PathParam("id") long id) {
    	 Response.ResponseBuilder builder = null;
    	try {
    		ProductRepository.remove(id); 
    		 builder = Response.ok();
    	}catch(Exception e) {
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
    	}
        return builder.build();
    }                   

    
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("saveProduct/{name}/{description}")
    public Response saveProduct(@PathParam("name") String name,
        @PathParam("description") String description){
    	Response.ResponseBuilder builder = null;
    	Product product = new Product(name,description);
        try {
            ProductRepository.save(product);
            builder = Response.ok();
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
      
    }
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("editProduct/{id}/{name}/{description}")
    public Response editProduct(@PathParam("id") String id, @PathParam("name") String name,
        @PathParam("description") String description){
    
    	Response.ResponseBuilder builder = null;
        try {
        	Product productForEdition = ProductRepository.findById(Long.valueOf(id));
        	productForEdition.setName(name);
        	productForEdition.setDescription(description);
            ProductRepository.save(productForEdition);
            builder = Response.ok();
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
      
    }
    
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("saveImage/{idProduct}/{type}")
    public Response saveImage(@PathParam("type") String type,
        @PathParam("idProduct") String idProduct){
    
    	Response.ResponseBuilder builder = null;
        try {
        	Image image = new Image(type);
        	image.setProduct(new Product(Long.valueOf(idProduct)));
        	imageRepository.save(image);
            builder = Response.ok();
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
      
    }
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("editImage/{id}/{type}/{idProduct}")
    public Response editImage(@PathParam("id") String id,@PathParam("type") String type,
    		
        @PathParam("idProduct") String idProduct){
    
    	Response.ResponseBuilder builder = null;
        try {
        	Image imageForEdition = imageRepository.findById(Long.parseLong(id));
        	imageForEdition.setProduct(new Product(Long.valueOf(idProduct)));
        	imageForEdition.setType(type);
        	imageRepository.save(imageForEdition);
            builder = Response.ok();
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
      
    }

    @GET
    @Path("removeImagemById/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeImagem(@PathParam("id") long id) {
    	 Response.ResponseBuilder builder = null;
    	try {
    		imageRepository.remove(id); 
    		 builder = Response.ok();
    	}catch(Exception e) {
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
    	}
        return builder.build();
    }   

}

package br.com.ac.teste.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import br.com.ac.teste.model.Image;
import br.com.ac.teste.model.Product;
@XmlRootElement
public class ProductDTO {
	public ProductDTO(Long id,String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}
	public ProductDTO(Long id,String name, String description,List<Product> subProducts,List<Image>images) {
	
		this.id = id;
		this.name = name;
		this.description = description;
		this.images = images;
		this.subProducts = subProducts;
	
	}
	private Long id;
	private String name;
	private String description;
	private Product produto;
	private List<Product> subProducts;
	private List<Image> images;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Product getProduto() {
		return produto;
	}
	public void setProduto(Product produto) {
		this.produto = produto;
	}
	public List<Product> getSubProducts() {
		return subProducts;
	}
	public void setSubProducts(List<Product> subProducts) {
		this.subProducts = subProducts;
	}
	public List<Image> getImages() {
		return images;
	}
	public void setImages(List<Image> images) {
		this.images = images;
	}
	
}

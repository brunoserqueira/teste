package br.com.ac.teste.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
@Entity
@NamedQuery(name = "Image.getImagesByProduct", query = "SELECT i FROM Image i WHERE i.product.id = :idProduct")
public class Image {
	public Image() {
	}
	
	public Image(String type) {
		
		this.type = type;
	}
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	@Column(name="ID")
	private Long id;
	@Column(name="TYPE")
	private String type;
	@JoinColumn(name="ID_PRODUCT")
	@ManyToOne
	
	private Product product;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	 
}

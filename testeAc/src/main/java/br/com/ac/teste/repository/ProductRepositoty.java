package br.com.ac.teste.repository;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.ac.teste.dto.ProductDTO;
import br.com.ac.teste.model.Product;

@Stateless
@Local
public class ProductRepositoty {

	@PersistenceContext(unitName = "hsqldb")
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public ProductDTO findByIdWithDependencies(Long id) {
 		
	Query query = em.createQuery(
			"SELECT NEW br.com.ac.teste.dto.ProductDTO(p.id,p.name,p.description) FROM Product p WHERE p.id = :id");
	query.setParameter("id", id);
	ProductDTO product = (ProductDTO) query.getSingleResult();

	product.setImages(em.createNamedQuery("Image.getImagesByProduct").setParameter("idProduct", product.getId())
			.getResultList());
	product.setSubProducts(em.createNamedQuery("Product.getProductChildrenByIdProduct")
			.setParameter("idProdutoPai", product.getId()).getResultList());
	return product;

	}
	 public Product findById(Long id) {
	        return em.find(Product.class, id);
	    }
	 
	@SuppressWarnings("unchecked")
	public ProductDTO findByIdWithImagem(Long id) {
		 		
		Query query = em.createQuery(
				"SELECT NEW br.com.ac.teste.dto.ProductDTO(p.id,p.name,p.description) FROM Product p WHERE p.id = :id");
		query.setParameter("id", id);
		ProductDTO product = (ProductDTO) query.getSingleResult();

		product.setImages(em.createNamedQuery("Image.getImagesByProduct").setParameter("idProduct", product.getId())
				.getResultList());

		return product;

	}

	@SuppressWarnings("unchecked")
	public ProductDTO findByIdWithSubProducts(Long id) {
		Query query = em.createQuery(
				"SELECT NEW br.com.ac.teste.dto.ProductDTO(p.id,p.name,p.description) FROM Product p WHERE p.id = :id");
		query.setParameter("id", id);
		ProductDTO product = (ProductDTO) query.getSingleResult();

		product.setSubProducts(em.createNamedQuery("Product.getProductChildrenByIdProduct")
				.setParameter("idProdutoPai", product.getId()).getResultList());

		return product;

	}

	@SuppressWarnings("unchecked")
	public List<ProductDTO> findAll() {

		Query query = em.createQuery(
				"SELECT NEW br.com.ac.teste.dto.ProductDTO(p.id,p.name,p.description) FROM Product p");
		List<ProductDTO> products = query.getResultList();
		for (ProductDTO product : products) {
			product.setImages(em.createNamedQuery("Image.getImagesByProduct").setParameter("idProduct", product.getId())
					.getResultList());
			product.setSubProducts(em.createNamedQuery("Product.getProductChildrenByIdProduct")
					.setParameter("idProdutoPai", product.getId()).getResultList());
		}
		return products;

	}

	@SuppressWarnings("unchecked")
	public List<ProductDTO> findAllWithImages() {
		Query query = em.createQuery(
				"SELECT NEW br.com.ac.teste.dto.ProductDTO(p.id,p.name,p.description) FROM Product p");
		List<ProductDTO> products = query.getResultList();
		for (ProductDTO product : products) {
			product.setImages(em.createNamedQuery("Image.getImagesByProduct").setParameter("idProduct", product.getId())
					.getResultList());
		 
		}
		return products;

	}

	@SuppressWarnings("unchecked")
	public List<ProductDTO> findAllWithSubProducts() {
		Query query = em.createQuery(
				"SELECT NEW br.com.ac.teste.dto.ProductDTO(p.id,p.name,p.description) FROM Product p");
		List<ProductDTO> products = query.getResultList();
		for (ProductDTO product : products) {
			product.setSubProducts(em.createNamedQuery("Product.getProductChildrenByIdProduct")
					.setParameter("idProdutoPai", product.getId()).getResultList());
		 
		}
		return products;

	}
	@SuppressWarnings("unchecked")
	public List<Product> findAllChildrenByIdProduct(Long idProduct) {
		Query query = em.createNamedQuery("Product.getProductChildrenByIdProduct");
		query.setParameter("idProduct",idProduct);
		List<Product> productChildren = query.getResultList();
		 
		return productChildren;

	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Product save(Product product) {
		Product productSaved = em.merge(product);
		return productSaved;

	}

	public void remove(Long id) {
		Product productToRemove = em.find(Product.class, id);
		em.remove(productToRemove);
	}

	
}

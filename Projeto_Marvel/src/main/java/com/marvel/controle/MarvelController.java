package com.marvel.controle;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.codec.digest.DigestUtils;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;


@Controller()
public class MarvelController {
	 private static final String PUBLIC_KEY="dd490cd66270830f5578fd42f3dac13c";
	 private static final String PRIVATE_KEY="b42cbdb39c3cd4eddb89baf9b10772a59f2a638b";
	
	RestTemplate restTemplate = new RestTemplate();
	
	private String recuperarSeriesLimitandoDataInicial(Long ano){
		Client client = Client.create();
		long timeStamp = System.currentTimeMillis();
	    String stringToHash = timeStamp + PUBLIC_KEY + PRIVATE_KEY;
	    String hash = DigestUtils.md5Hex(stringToHash);

	    String url =String.format("https://gateway.marvel.com:443/v1/public/series?startYear=2013&ts=%d&apikey=%s&hash=%s",timeStamp ,PUBLIC_KEY, hash);

	    WebResource webResource = client.resource(url);
		ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
		
		String output = response.getEntity(String.class);
//		SuperHeroi output = response.getEntity(SuperHeroi.class);
		return output;
	}
	/**
	 * M�todo provis�rio para adicionar resposta para o objeto especifico
	 * erro no processo de retornar diretamente o Objeto especifico com json
	 * @param ano
	 * @return
	 * 
	 */
	@SuppressWarnings("unused")
	private List<String> construirListaComics(String retorno){
		return null;
		 
	}
 
	@Test
	public void testarCredencialInvalidaServicoSeries(){
		Long anoTeste = 2012L;
		Assert.assertThat(recuperarSeriesLimitandoDataInicial(anoTeste), CoreMatchers.containsString("InvalidCredentials"));

	}
	private List<String> personagensMock(){
		List<String> personagens = new ArrayList<String>();
		for(int x= 0;x <10;x++){
			personagens.add("Personagem N� "+x);
		}
		return personagens;
	}
	@Test
	public void testarErroRequisicaoServicoSeries(){
		Long codigoErroServicoMarvel=409L;
		Long anoTeste = 2012L;
		Assert.assertTrue(!recuperarSeriesLimitandoDataInicial(anoTeste).contains(codigoErroServicoMarvel.toString()));

	}
	private String recuperarComicsLimitandoDataInicial(Long ano){
		Client client = Client.create();
		long timeStamp = System.currentTimeMillis();
	    
	    String stringToHash = timeStamp + PUBLIC_KEY + PRIVATE_KEY;
	       
//	    tring hash = Math.random();
	    String hash = DigestUtils.md5Hex(stringToHash);
//		WebResource webResource = client.resource("http://gateway.marvel.com/v1/public/characters/1009165?apikey=dd490cd66270830f5578fd42f3dac13c");
		
	    WebResource webResource = client.resource("http://gateway.marvel.com/v1/public/comics?startYear="+ano+"&hash="+hash+"&ts="+timeStamp+"&apikey="+PUBLIC_KEY);
		
	    ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
	   
		String output = response.getEntity(String.class);
//		SuperHeroi output = response.getEntity(SuperHeroi.class);
		return output;
	}
	@RequestMapping(value = "/recuperarDadosMarvel", method = RequestMethod.POST)
	public String resultado(Long ano,Model model) {
		recuperarSeriesLimitandoDataInicial(ano);
		recuperarComicsLimitandoDataInicial(ano);
		recuperarPersonagensMarvel();
	
		model.addAttribute("lista", personagensMock() );
		
		return "resultado";
	}
 
	private String recuperarPersonagensMarvel(){
	 long timeStamp = System.currentTimeMillis();
       int limit = 5;

       String stringToHash = timeStamp + "b42cbdb39c3cd4eddb89baf9b10772a59f2a638b" + "dd490cd66270830f5578fd42f3dac13c";
       String hash = DigestUtils.md5Hex(stringToHash);
       String url = String.format("http://gateway.marvel.com/v1/public/characters/1009189?ts=%d&apikey=%s&hash=%s&limit=%d", timeStamp, "dd490cd66270830f5578fd42f3dac13c", hash, limit);
       Client client = Client.create();
       WebResource webResource = client.resource(url);
	    ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
//	    response.getEntity(SuperHeroi.class);
//	    JSONObject outpuet;
//	    try {
//	    	 outpuet = new JSONObject(response.getEntity(String.class));
//	    	 
//	    	 ((JSONObject)outpuet.get("data")).get("results");
//	    } catch (ClientHandlerException e) {
//			e.printStackTrace();
//		} catch (UniformInterfaceException e) {
//			e.printStackTrace();
//		} catch (JSONException e) {
//			e.printStackTrace();
//		} 
	    String output = response.getEntity(String.class);
//	    SuperHeroi output = response.getEntity(SuperHeroi.class);
		return output;
}
	@RequestMapping(value = "/marvel", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		

		
		
		return "home";
	}
	
}

package com.marvel.modelo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class SuperHeroi  implements Serializable {

	private static final long serialVersionUID = -5840768685912166961L;
	
	private Integer id;
    private String code;
//    private Value value;

    public SuperHeroi() {
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "SuperHeroi [id=" + id + ", code=" + code + "]";
	}


}
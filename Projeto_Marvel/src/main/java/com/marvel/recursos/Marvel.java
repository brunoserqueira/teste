package com.marvel.recursos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.marvel.modelo.SuperHeroi;

@ApplicationPath("/personagens")
public class Marvel {

	//N�o consegui subir com o jersey no web.xml: para registrar o servi�o e consumir da api marvel.
	/**
	
<!-- 	<servlet> -->
<!--     <servlet-name>Jersey REST Service</servlet-name> -->
<!--     <servlet-class> com.sun.jersey.spi.container.servlet.ServletContainer</servlet-class> -->
<!--      Register resources and providers under com.vogella.jersey.first package. -->
<!--     <init-param> -->
<!--         <param-name>com.sun.jersey.config.property.packages</param-name> -->
<!--         <param-value>com.marvel.recursos</param-value> -->
<!--     </init-param> -->
<!--     <load-on-startup>1</load-on-startup> -->
<!--   </servlet> -->
	 */
	static private Map<Integer, SuperHeroi> marvelMap;

	static {
		marvelMap = new HashMap<Integer, SuperHeroi>();

		SuperHeroi s1 = new SuperHeroi();
		s1.setCode("1");
		s1.setId(1);
		marvelMap.put(s1.getId(), s1);

		SuperHeroi s2 = new SuperHeroi();
		s2.setCode("2");
		s2.setId(1);

		marvelMap.put(s2.getId(), s2);
	}

	@GET
	@Produces("text/xml")
	public List<SuperHeroi> getSuperHerois() {
		return new ArrayList<SuperHeroi>(marvelMap.values());
	}
}